package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.create.CreateXMLDocument;
import org.xml.sax.XMLReader;


import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);

		domController.buildListStudents();

		// sort (case 1)
		// PLACE YOUR CODE HERE

		// save
		String outputXmlFile = "output.dom.xml";

		CreateXMLDocument.createDocument(outputXmlFile, domController.getFlowers());

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		XMLReader reader = parser.getXMLReader();
		reader.setContentHandler(saxController);

		reader.parse(saxController.getXmlFileName());

		// sort  (case 2)
		// PLACE YOUR CODE HERE

		// save
		outputXmlFile = "output.sax.xml";

		CreateXMLDocument.createDocument(outputXmlFile, domController.getFlowers());

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);

		staxController.buildFlowers();

		// sort  (case 3)
		// PLACE YOUR CODE HERE

		// save
		outputXmlFile = "output.stax.xml";
		CreateXMLDocument.createDocument(outputXmlFile, domController.getFlowers());
	}

}
