package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerXmlTag;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private List<Flower> flowers;
	private DocumentBuilder docBuilder;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new ArrayList<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public List<Flower> getFlowers() {
		return flowers;
	}

	public void buildListStudents() {
		Document doc;
		try {
			doc = docBuilder.parse(xmlFileName);
			Element root = doc.getDocumentElement();

			NodeList flowerList = root.getElementsByTagName("flower");
			for (int i = 0; i < flowerList.getLength(); i++) {
				Element flowerElement = (Element) flowerList.item(i);
				Flower flower = buildFlower(flowerElement);
				flowers.add(flower);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	private Flower buildFlower(Element flowerElement) {
		Flower flower = new Flower();
		if (flowerElement != null) {
			flower.setName(getElementTextContent(flowerElement, FlowerXmlTag.NAME.getValue()));
			flower.setSoil(getElementTextContent(flowerElement, FlowerXmlTag.SOIL.getValue()));
			flower.setOrigin(getElementTextContent(flowerElement, FlowerXmlTag.ORIGIN.getValue()));

			Flower.VisualParameters visualParameters = flower.getVisualParameters();
			Element visParamElement = (Element) flowerElement.getElementsByTagName(FlowerXmlTag.VISUALPARAMETERS.getValue()).item(0);

			visualParameters.setStemColor(getElementTextContent(visParamElement, FlowerXmlTag.STEMCOLOUR.getValue()));
			visualParameters.setAveLenFlower(getElementTextContent(visParamElement, FlowerXmlTag.AVELENFLOWER.getValue()));
			visualParameters.setLeafColour(getElementTextContent(visParamElement, FlowerXmlTag.LEAFCOLOUR.getValue()));

			Flower.GrowingTips growingTips = flower.getGrowingTips();
			Element growTipsElement = (Element) flowerElement.getElementsByTagName(FlowerXmlTag.GROWINGTIPS.getValue()).item(0);

			growingTips.setTemperature(getElementTextContent(growTipsElement, FlowerXmlTag.TEMPRETURE.getValue()));
			growingTips.setLighting("yes");
			growingTips.setWatering(getElementTextContent(growTipsElement, FlowerXmlTag.WATERING.getValue()));

			flower.setMultiplying(getElementTextContent(flowerElement, FlowerXmlTag.MULTIPLYING.getValue()));

		}

		return flower;
	}

	private static String getElementTextContent(Element element, String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		return node.getTextContent();
	}
}
