package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerXmlTag;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;
	private List<Flower> flowers;
	private Flower current;
	private FlowerXmlTag currentXMLTag;
	private EnumSet<FlowerXmlTag> withText;
	private static final String ELEMENT_FLOWER = "flower";

	public SAXController(String xmlFileName) {
		flowers = new ArrayList<>();
		this.xmlFileName = xmlFileName;
		withText = EnumSet.range(FlowerXmlTag.NAME, FlowerXmlTag.MULTIPLYING);
	}

	public String getXmlFileName() {
		return xmlFileName;
	}

	public List<Flower> getFlowers() {
		return flowers;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (ELEMENT_FLOWER.equals(qName))
			current = new Flower();
		else if (FlowerXmlTag.LIGHTING.getValue().equals(qName))
			current.getGrowingTips().setLighting(attributes.getValue(0));
		else {
			FlowerXmlTag temp = FlowerXmlTag.valueOf(qName.toUpperCase());
			if (withText.contains(temp))
				currentXMLTag = temp;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String data = new String(ch, start, length);
		if (currentXMLTag != null) {
			switch (currentXMLTag) {
				case NAME:
					current.setName(data);
					break;
				case SOIL:
					current.setSoil(data);
					break;
				case ORIGIN:
					current.setOrigin(data);
					break;
				case STEMCOLOUR:
					current.getVisualParameters().setStemColor(data);
					break;
				case LEAFCOLOUR:
					current.getVisualParameters().setLeafColour(data);
					break;
				case AVELENFLOWER:
					current.getVisualParameters().setAveLenFlower(data);
					break;
				case TEMPRETURE:
					current.getGrowingTips().setTemperature(data);
					break;
				case LIGHTING:
					current.getGrowingTips().setLighting(data);
					break;
				case WATERING:
					current.getGrowingTips().setWatering(data);
					break;
				case MULTIPLYING:
					current.setMultiplying(data);
					break;
			}

			currentXMLTag = null;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (ELEMENT_FLOWER.equals(qName))
			flowers.add(current);
	}
}