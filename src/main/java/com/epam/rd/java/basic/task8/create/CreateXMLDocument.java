package com.epam.rd.java.basic.task8.create;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerXmlTag;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CreateXMLDocument {
    public static void createDocument(String fileName, List<Flower> flowers) {
        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();

        documentBuilderFactory.setNamespaceAware(true);

        DocumentBuilder builder = null;

        try {
            builder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Document document = builder.newDocument();

        Element rootElement = document.createElement(FlowerXmlTag.FLOWERS.getValue());
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        document.appendChild(rootElement);

        for (Flower flower : flowers) {
            Element flowerElement = document.createElement(FlowerXmlTag.FLOWER.getValue());
            rootElement.appendChild(flowerElement);

            Element nameElement = document.createElement(FlowerXmlTag.NAME.getValue());
            nameElement.appendChild(document.createTextNode(flower.getName()));
            flowerElement.appendChild(nameElement);

            Element soilElement = document.createElement(FlowerXmlTag.SOIL.getValue());
            soilElement.appendChild(document.createTextNode(flower.getSoil()));
            flowerElement.appendChild(soilElement);

            Element originElement = document.createElement(FlowerXmlTag.ORIGIN.getValue());
            originElement.appendChild(document.createTextNode(flower.getOrigin()));
            flowerElement.appendChild(originElement);

            /**
             *  Create visual parameters
             */

            Element visualElement = document.createElement(FlowerXmlTag.VISUALPARAMETERS.getValue());

            Element stemElement = document.createElement(FlowerXmlTag.STEMCOLOUR.getValue());
            stemElement.appendChild(document.createTextNode(flower.getVisualParameters().getStemColor()));
            visualElement.appendChild(stemElement);

            Element leafElement = document.createElement(FlowerXmlTag.LEAFCOLOUR.getValue());
            leafElement.appendChild(document.createTextNode(flower.getVisualParameters().getLeafColour()));
            visualElement.appendChild(leafElement);

            Element aveLenElement = document.createElement(FlowerXmlTag.AVELENFLOWER.getValue());
            aveLenElement.setAttribute("measure", "cm");
            aveLenElement.appendChild(document.createTextNode(flower.getVisualParameters().getAveLenFlower()));
            visualElement.appendChild(aveLenElement);



            flowerElement.appendChild(visualElement);

            /**
             * Create growing tips
             */
            Element tipsElement = document.createElement(FlowerXmlTag.GROWINGTIPS.getValue());

            Element tempretureElement = document.createElement(FlowerXmlTag.TEMPRETURE.getValue());
            tempretureElement.setAttribute("measure", "celcius");
            tempretureElement.appendChild(document.createTextNode(flower.getGrowingTips().getTemperature()));
            tipsElement.appendChild(tempretureElement);

            Element lightingElement = document.createElement(FlowerXmlTag.LIGHTING.getValue());
            lightingElement.setAttribute("lightRequiring", flower.getGrowingTips().getLighting());
            tipsElement.appendChild(lightingElement);

            Element wateringElement = document.createElement(FlowerXmlTag.WATERING.getValue());
            wateringElement.setAttribute("measure", "mlPerWeek");
            wateringElement.appendChild(document.createTextNode(flower.getGrowingTips().getWatering()));
            tipsElement.appendChild(wateringElement);

            flowerElement.appendChild(tipsElement);

            Element multiplyingElement = document.createElement(FlowerXmlTag.MULTIPLYING.getValue());
            multiplyingElement.appendChild(document.createTextNode(flower.getMultiplying()));
            flowerElement.appendChild(multiplyingElement);
        }

        TransformerFactory factory = TransformerFactory.newInstance();

        try {
            Transformer transformer = factory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(fileName));
            transformer.transform(source, result);

        } catch (TransformerConfigurationException | IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
