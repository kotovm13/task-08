package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.utils.FlowerXmlTag;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Flower> flowers;
	private XMLInputFactory inputFactory;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		inputFactory = XMLInputFactory.newInstance();
		flowers = new ArrayList<>();
	}

	public List<Flower> getFlowers() {
		return flowers;
	}

	public void buildFlowers() {
		XMLStreamReader reader;
		String name;

		try (FileInputStream inputStream = new FileInputStream(new File(xmlFileName))) {
			reader = inputFactory.createXMLStreamReader(inputStream);

			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					if (name.equals(FlowerXmlTag.FLOWER.getValue())) {
						Flower flower = buildFlower(reader);
						flowers.add(flower);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	private Flower buildFlower(XMLStreamReader reader) throws XMLStreamException {
		Flower flower = new Flower();
		String name;

		while (reader.hasNext()) {
			int type = reader.next();

			switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();

					switch (FlowerXmlTag.valueOf(name.toUpperCase())) {
						case NAME:
							flower.setName(getXMLText(reader));
							break;
						case SOIL:
							flower.setSoil(getXMLText(reader));
							break;
						case ORIGIN:
							flower.setOrigin(getXMLText(reader));
							break;
						case GROWINGTIPS:
							flower.setGrowingTips(getXMLTips(reader));
							break;
						case VISUALPARAMETERS:
							flower.setVisualParameters(getXMLParams(reader));
							break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (FlowerXmlTag.valueOf(name.toUpperCase()) == FlowerXmlTag.FLOWER)
						return flower;
			}
		}

		throw new XMLStreamException("Unknown element in tag <flower>");
	}

	private Flower.VisualParameters getXMLParams(XMLStreamReader reader) throws XMLStreamException {
		Flower.VisualParameters parameters = new Flower().new VisualParameters();
		int type;
		String name;

		while (reader.hasNext()) {
			type = reader.next();

			switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();

					switch (FlowerXmlTag.valueOf(name.toUpperCase())) {
						case STEMCOLOUR:
							parameters.setStemColor(getXMLText(reader));
							break;
						case LEAFCOLOUR:
							parameters.setLeafColour(getXMLText(reader));
							break;
						case AVELENFLOWER:
							parameters.setAveLenFlower(getXMLText(reader));
							break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (FlowerXmlTag.valueOf(name.toUpperCase()) == FlowerXmlTag.VISUALPARAMETERS)
						return parameters;
			}
		}

		throw new XMLStreamException("Unknown element in tag <visualParameters>");
	}

	private Flower.GrowingTips getXMLTips(XMLStreamReader reader) throws XMLStreamException {
		Flower.GrowingTips tips = new Flower().new GrowingTips();
		int type;
		String name;

		while (reader.hasNext()) {
			type = reader.next();

			switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					name = reader.getLocalName();

					switch (FlowerXmlTag.valueOf(name.toUpperCase())) {
						case TEMPRETURE:
							tips.setTemperature(getXMLText(reader));
							break;
						case LIGHTING:
							tips.setLighting("yes");
							break;
						case WATERING:
							tips.setWatering(getXMLText(reader));
							break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					name = reader.getLocalName();
					if (FlowerXmlTag.valueOf(name.toUpperCase()) == FlowerXmlTag.GROWINGTIPS)
						return tips;
			}
		}
		throw new XMLStreamException("Unknown element in tag <growingTips>");
	}

	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}

		return text;
	}
}