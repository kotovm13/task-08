package com.epam.rd.java.basic.task8.model;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters = new VisualParameters();
    private GrowingTips growingTips = new GrowingTips();
    private String multiplying;

    public Flower() {
    }

    public Flower(String name, String soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Flower{");
        sb.append("name='").append(name).append('\'');
        sb.append(", soil='").append(soil).append('\'');
        sb.append(", origin='").append(origin).append('\'');
        sb.append(", visualParameters=").append(visualParameters);
        sb.append(", growingTips=").append(growingTips);
        sb.append(", multiplying='").append(multiplying).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public class VisualParameters {
        private String stemColor;
        private String leafColour;
        private String aveLenFlower;

        public VisualParameters() {
        }

        public VisualParameters(String stemColor, String leafColour, String aveLenFlower) {
            this.stemColor = stemColor;
            this.leafColour = leafColour;
            this.aveLenFlower = aveLenFlower;
        }

        public String getStemColor() {
            return stemColor;
        }

        public void setStemColor(String stemColor) {
            this.stemColor = stemColor;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public String getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(String aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("VisualParameters{");
            sb.append("stemColor='").append(stemColor).append('\'');
            sb.append(", leafColour='").append(leafColour).append('\'');
            sb.append(", aveLenFlower='").append(aveLenFlower).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    public class GrowingTips {
        private String temperature;
        private String lighting;
        private String watering;

        public GrowingTips() {
        }

        public GrowingTips(String temperature, String lighting, String watering) {
            this.temperature = temperature;
            this.lighting = lighting;
            this.watering = watering;
        }

        public String getTemperature() {
            return temperature;
        }

        public void setTemperature(String temperature) {
            this.temperature = temperature;
        }

        public String getLighting() {
            return lighting;
        }

        public void setLighting(String lighting) {
            this.lighting = lighting;
        }

        public String getWatering() {
            return watering;
        }

        public void setWatering(String watering) {
            this.watering = watering;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("GrowingTips{");
            sb.append("temperature='").append(temperature).append('\'');
            sb.append(", lighting='").append(lighting).append('\'');
            sb.append(", watering='").append(watering).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}
