package com.epam.rd.java.basic.task8.utils;

public enum FlowerXmlTag {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    TEMPRETURE("tempreture"),
    LIGHTING("lighting"),
    WATERING("watering"),
    MULTIPLYING("multiplying"),
    VISUALPARAMETERS("visualParameters"),
    GROWINGTIPS("growingTips");


    private String value;

    FlowerXmlTag(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
